/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satellitepredictorserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peter
 */
public class SatellitePredictorServer implements Runnable{
	private ServerSocket serverSocket;
	private boolean quit;



	@SuppressWarnings("empty-statement")
	public static void main(String[] args) {
		SatellitePredictorServer server;
		try {
			server = new SatellitePredictorServer();
		} catch (IOException ex) {
			Logger.getLogger(SatellitePredictorServer.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}

		Thread serverThread = new Thread(server);
		serverThread.start();

		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		try {
			while(!keyboard.readLine().equalsIgnoreCase("quit"))
				;
		} catch (IOException ex) {
			Logger.getLogger(SatellitePredictorServer.class.getName()).log(Level.SEVERE, null, ex);
		}
		server.quit();
		try {
			serverThread.join();
		} catch (InterruptedException ex) {
			Logger.getLogger(SatellitePredictorServer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}



	public SatellitePredictorServer() throws IOException{
		quit         = false;
		serverSocket = new ServerSocket(8080);
	}



	public void quit(){
		this.quit = true;
	}


	@Override
	public void run() {
		System.out.println("satellite predictor server started");
		try {
			serverSocket.setSoTimeout(1000);
		} catch (SocketException ex) {
			System.out.println("can't set serverSocket.accept() timeout");
		}

		while(!quit) {
			Socket sessionSocket;
			try {
				sessionSocket = serverSocket.accept();
			} catch (IOException ex) {
				//System.out.println("server alive - waiting for connection");
				continue;
			}

			try {
				PredictorSession session = new PredictorSession(sessionSocket);
				(new Thread(session)).start();
			} catch (IOException ex) {
				Logger.getLogger(SatellitePredictorServer.class.getName()).log(Level.SEVERE, null, ex);
				continue;
			}
		}

		System.out.println("stopping server");
		try {
			serverSocket.close();
		} catch (IOException ex) {
			Logger.getLogger(SatellitePredictorServer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
