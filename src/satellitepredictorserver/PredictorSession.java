/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package satellitepredictorserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.me.g4dpz.satellite.*;


/*

49 21.25 250
ISS (ZARYA)
1 25544U 98067A   12283.42674131  .00010470  00000-0  18627-3 0  4370
2 25544  51.6477 273.7627 0018032 146.9011  13.6697 15.50596635795763

 */
/**
 *
 * @author peter
 */
public class PredictorSession implements Runnable{

	private Socket socket;
	private BufferedReader reader;
	private PrintWriter printWriter;

	private GroundStationPosition groundStation;
	private TLE tle;
	private PassPredictor predictor;
	private Date time;

	private boolean quit;


	public PredictorSession(Socket socket) throws IOException{
		this.socket = socket;
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		printWriter = new PrintWriter(socket.getOutputStream(), true);
	}



	@Override
	public void run() {
		System.out.println("new connection accepted");
		if(!preparePredictor()){
			closeConnection();
			return;
		}

		quit = false;
		while(!quit){
			String line[];
			try {
				line = readLine();
				if(line == null || line.length < 1) {
					continue;
				}
			} catch (IOException ex) {
				if(!socket.isConnected()) {
					break;
				}
				continue;
			}

			try {
				processLine(line);
			} catch (InvalidTleException ex) {
				Logger.getLogger(PredictorSession.class.getName()).log(Level.SEVERE, null, ex);
				printWriter.println(ex.getLocalizedMessage());
				continue;
			} catch (SatNotFoundException ex) {
				Logger.getLogger(PredictorSession.class.getName()).log(Level.SEVERE, null, ex);
				printWriter.println(ex.getLocalizedMessage());
				continue;
			} catch (IllegalArgumentException ex) {
				Logger.getLogger(PredictorSession.class.getName()).log(Level.SEVERE, null, ex);
				printWriter.println(ex.getLocalizedMessage());
				continue;
			}
			printWriter.println("OK");
		}

		closeConnection();
	}



	private void processLine(String []line) throws InvalidTleException, SatNotFoundException, IllegalArgumentException{
		if(line[0].equals("get_pos")){ // GET_POS
			if(line.length != 1) {
				throw new IllegalArgumentException("bad parameters count");
			}
			SatPos satPos = predictor.getSatPos(time);
			printWriter.println(
					satPos.getAzimuth() + " " +
					satPos.getElevation() + " " +
					satPos.getLatitude() + " " +
					satPos.getLongitude() + " " +

					satPos.getRange() + " " +
					satPos.getRangeRate() + " " +
					satPos.getPhase() + " " +
					satPos.getAltitude() + " " +
					satPos.getTheta() + " " +
					(satPos.isAboveHorizon() ? "true" : "false")
					);
			return;

		} else if(line[0].equals("set_time")){ // SET_TIME
			if(line.length != 2) {
				throw new IllegalArgumentException("bad parameters count");
			}

			if(line[1].equals("now")){
				time = new Date();
				return;
			}

			try{
				time = new Date(Long.parseLong(line[1])*1000l);
			} catch(NumberFormatException ex){
				throw new IllegalArgumentException("set_time parameter is not number");
			}
			return;

		} else if(line[0].equals("get_next_pass")){ // GET_NEXT_PASS
			if(line.length != 1) {
				throw new IllegalArgumentException("bad parameters count");
			}
			SatPassTime nextSatPass = predictor.nextSatPass(time);
			printWriter.println(
					nextSatPass.getStartTime().getTime()/1000 + " " +
					nextSatPass.getEndTime().getTime()/1000
					);
			return;

		} else if(line[0].equals("get_frequency")){ // GET_FREQUENCY
			if(line.length != 2) {
				throw new IllegalArgumentException("bad parameters count");
			}

			long frequency;
			try{
				frequency = Long.parseLong(line[1]);
			} catch(NumberFormatException ex){
				throw new IllegalArgumentException("set_time parameter is not number");
			}

			printWriter.println(
					predictor.getDownlinkFreq(frequency, time) + " " +
					predictor.getUplinkFreq(frequency, time)
					);
			return;

		} else if(line[0].equals("quit")){ // QUIT
			if(line.length != 1) {
				throw new IllegalArgumentException("bad parameters count");
			}
			quit = true;
			return;
		}

		throw new IllegalArgumentException("command not recognized");
	}



	private boolean preparePredictor(){
		groundStation = readGroundStation();
		if(groundStation == null){
			System.out.println("can't read ground station");
			return false;
		}

		tle = readTLE();
		if(tle == null){
			System.out.println("can't read TLE");
			return false;
		}
		System.out.println("data ok");

		try{
			predictor = new PassPredictor(tle, groundStation);
		} catch(IllegalArgumentException ex){
			System.out.println(ex.getLocalizedMessage());
			return false;
		}

		time = new Date();
		printWriter.println("OK");
		return true;
	}


	private void closeConnection(){
		try {
			System.out.println("closing connection");
			if(socket != null) {
				socket.close();
			}
			if(reader != null) {
				reader.close();
			}
			if(printWriter != null) {
				printWriter.close();
			}
		} catch (IOException ex) {
			Logger.getLogger(PredictorSession.class.getName()).log(Level.SEVERE, null, ex);
		}
	}



	private String[] readLine() throws IOException{
		return reader.readLine().split("\\s");
	}



	private GroundStationPosition readGroundStation(){
		System.out.println("reading ground station position");

		GroundStationPosition station;
		double latitude, longitude, height;

		try {
			String line[] = readLine();

			if(line.length != 3){
				System.out.println("bad ground station position definition");
				return null;
			}

			latitude  = Double.valueOf(line[0]);
			longitude = Double.valueOf(line[1]);
			height    = Double.valueOf(line[2]);
		} catch (NumberFormatException ex){
			System.out.println(ex.getLocalizedMessage());
			return null;
		} catch (IOException ex) {
			System.out.println(ex.getLocalizedMessage());
			return null;
		}

		return new GroundStationPosition(latitude, longitude, height);
	}



	private TLE readTLE(){
		System.out.println("reading TLE data");

		String lines[] = new String[3];
		for(int i=0; i<3; i++){
			try {
				lines[i] = reader.readLine();
			} catch (IOException ex) {
				Logger.getLogger(PredictorSession.class.getName()).log(Level.SEVERE, null, ex);
				return null;
			}
		}

		try{
			return new TLE(lines);
		} catch(IllegalArgumentException ex){
			System.out.println(ex.getLocalizedMessage());
			return null;
		}
	}
}
